#include "driver/adc.h"
#include "esp_adc_cal.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "math.h"

#define MEASURE "MAIN"

adc_channel_t voltage_pins[] = {ADC1_CHANNEL_0, ADC1_CHANNEL_3, ADC1_CHANNEL_6};
adc_channel_t current_pins[] = {ADC1_CHANNEL_2, ADC1_CHANNEL_5, ADC1_CHANNEL_8};

int voltage_bias[] = {4660, 4650, 4655};
int current_bias[] = {4620, 4633, 4590};

float voltage_scale[] = {308, 309, 307};
float current_scale[] = {696, 708, 692};

float voltage[] = {0, 0, 0};
float current[] = {0, 0, 0};

static const adc_atten_t atten = ADC_ATTEN_DB_11;

static const adc_bits_width_t width = ADC_WIDTH_BIT_13;
static const adc_unit_t unit = ADC_UNIT_1;

esp_adc_cal_characteristics_t *adc_chars;

#define DEFAULT_VREF 1100 //Use adc2_vref_to_gpio() to obtain a better estimate

int *voltage_buffer, *current_buffer;

int samples_per_cycle = 200;
portMUX_TYPE port_mutex = portMUX_INITIALIZER_UNLOCKED;

void init_adc()
{
    ESP_LOGI(MEASURE, "Initializing ADC");
    adc1_config_width(width);
    for (int phase = 0; phase < 3; phase++)
    {
        adc1_config_channel_atten(voltage_pins[phase], atten);
        adc1_config_channel_atten(current_pins[phase], atten);
    }
    adc_chars = calloc(1, sizeof(esp_adc_cal_characteristics_t));
    esp_adc_cal_value_t val_type = esp_adc_cal_characterize(unit, atten, width, DEFAULT_VREF, adc_chars);
    ESP_LOGI(MEASURE, "ADC Initialized");

    voltage_buffer = (int *)malloc(samples_per_cycle * sizeof(int));
    current_buffer = (int *)malloc(samples_per_cycle * sizeof(int));
}

void get_voltage_current(int phase)
{
    ESP_LOGI(MEASURE, "Statring Voltage and Current Measurment for Phase %d", phase + 1);

    voltage[phase] = adc1_get_raw(voltage_pins[phase]);
    current[phase] = adc1_get_raw(current_pins[phase]);

    ESP_LOGI(MEASURE, "Voltage and Current measurments at Phase %d are %f and %f", phase + 1, voltage[phase], current[phase]);

    // values.rms_voltage = voltage[phase];
    // values.rms_current = current[phase];
}

void measure_energy()
{
    init_adc();
    while (1)
    {
        for (int i = 0; i < 3; i++)
        {
            get_voltage_current(i);
            vTaskDelay(100 / portTICK_PERIOD_MS);
        }
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
}

void app_main()
{
    xTaskCreate(&measure_energy, "GET_ENEGRY_MEASUREMENTS", 4096 * 2, NULL, 1, NULL);
}
